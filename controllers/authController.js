const jwt = require("jsonwebtoken");

class AuthController {
  async getToken(req, res, next) {
    try {
      const body = {
        user: {
          id: req.user.id,
        },
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });

      return res.status(200).json({
        message: "Success",
        token,
      });
    } catch (e) {
      return next(e);
    }
  }

  async getTokenPartner(req, res) {
    try {
      const body = {
        partner: {
          id: req.partner.id,
        },
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });

      return res.status(200).json({
        message: "Success",
        token,
      });
    } catch (e) {
      return next(e);
    }
  }
  async getMe(req, res, next) {
    try {
      console.log(req.user);
      const data = await user.findOne({
        where: { id: req.user.id },
        attributes: { exclude: ["password"] },
      });

      return res.status(200).json({ data });
    } catch (error) {
      return next(error);
    }
  }
}

module.exports = new AuthController();
